/*
 * Copyright (C) 2022   Rodolfo Giometti <giometti@enneenne.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _LOG2CB_H
#define _LOG2CB_H

#define LOG2CB_VERSION            __VERSION

#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>
#include <asm/byteorder.h>

extern int __debug_level;
extern int __add_time;
extern const char *log2cb_error_str[];

/* The process error codes */
enum log2cb_perror_e {
        LOG2CB_PERROR_GENERIC = EXIT_FAILURE,
        LOG2CB_PERROR_CORRUPTED,
        __LOG2CB_PERROR_END
};

/* The error codes */
enum log2cb_error_e {
	LOG2CB_SUCCESS,
	LOG2CB_ERROR_ERRNO,
	LOG2CB_ERROR_CORRUPTED_LOGFILE,
	LOG2CB_ERROR_INVALID_MAGIC,
	LOG2CB_ERROR_INVALID_VERSION,
	LOG2CB_ERROR_INVALID_HEAD,
	LOG2CB_ERROR_INVALID_TAIL,
	LOG2CB_ERROR_INVALID_MODE,
	LOG2CB_ERROR_INVALID_MESSAGE,
	LOG2CB_ERROR_TOO_LONG_MESSAGE,
	LOG2CB_ERROR_INVALID_OFFSET,
	__LOG2CB_ERROR_END
};

#define log2cb_is_corrupted(e)	unlikely((e) == LOG2CB_ERROR_INVALID_MAGIC   ||\
					 (e) == LOG2CB_ERROR_INVALID_VERSION ||\
					 (e) == LOG2CB_ERROR_INVALID_HEAD    ||\
					 (e) == LOG2CB_ERROR_INVALID_TAIL)

/* The log file header */
struct log2cb_header_s {
	uint32_t magic;		/* read only */
#define LOG2CB_MAGIC		0xa51052cb
	uint32_t version;	/* read only */
#define LOG2CB_VERSION_1	(1 << 16)
	uint32_t maxsize;	/* maximum data storage: write once at creat. */
	uint32_t filesize;	/* just a counter (used for "tail -f" mode) */
	uint32_t head;		/* new data insert position */
	uint32_t tail;		/* the older message in the buffer */
	uint32_t pad[10];
} __packed;
#define LOG2CB_HEADER_SIZE	sizeof(struct log2cb_header_s)

#define __log2cb_set_field(fd, field, value)				\
        ({                                                              \
		int ret;						\
                typeof(((struct log2cb_header_s *)0)->field) d =	\
						__cpu_to_le32(value);	\
                ret = log2cb_pwriten(fd, &d, membersizeof(struct log2cb_header_s, field),\
                        offsetof(struct log2cb_header_s, field), 0);	\
		ret < 0 ? -LOG2CB_ERROR_ERRNO : ret;			\
        })

#define __log2cb_get_field(fd, field, pvalue)				\
        ({                                                              \
		int ret;						\
                typeof(((struct log2cb_header_s *)0)->field) d;		\
                ret = log2cb_preadn(fd, &d, membersizeof(struct log2cb_header_s, field),\
                        offsetof(struct log2cb_header_s, field), 0);	\
		*pvalue = __le32_to_cpu(d);				\
		ret < 0 ? -LOG2CB_ERROR_ERRNO : ret;			\
        })

/*
 * Exported functions
 */

#define __log2cb_set_magic(fd)						\
			__log2cb_set_field(fd, magic, LOG2CB_MAGIC)
#define __log2cb_set_version(fd)					\
			__log2cb_set_field(fd, version, LOG2CB_VERSION_1)
#define __log2cb_set_maxsize(fd, value)					\
			__log2cb_set_field(fd, maxsize, value)
#define __log2cb_set_filesize(fd, value)				\
			__log2cb_set_field(fd, filesize, value)
#define __log2cb_set_head(fd, value)					\
			__log2cb_set_field(fd, head, value)
#define __log2cb_set_tail(fd, value)					\
			__log2cb_set_field(fd, tail, value)
#define __log2cb_get_magic(fd, pvalue)					\
			__log2cb_get_field(fd, magic, pvalue)
#define __log2cb_get_version(fd, pvalue)				\
			__log2cb_get_field(fd, version, pvalue)
#define __log2cb_get_maxsize(fd, pvalue)				\
			__log2cb_get_field(fd, maxsize, pvalue)
#define __log2cb_get_filesize(fd, pvalue)				\
			__log2cb_get_field(fd, filesize, pvalue)
#define __log2cb_get_head(fd, pvalue)					\
			__log2cb_get_field(fd, head, pvalue)
#define __log2cb_get_tail(fd, pvalue)					\
			__log2cb_get_field(fd, tail, pvalue)

extern const char *log2cb_strerror(enum log2cb_error_e error);

extern int log2cb_preadn(int fd, void *vptr, size_t n, size_t off,
						unsigned long long t_ns);
extern int log2cb_pwriten(int fd, const void *vptr, size_t n, size_t off,
						unsigned long long t_ns);
extern int log2cb_write_lock(int fd);
extern int log2cb_write_unlock(int fd);
extern int log2cb_read_lock(int fd);
extern int log2cb_read_unlock(int fd);

/* The whence directive for __log2cb_read_data() */
#define LOG2CB_FROM_BEGINNING	0
#define LOG2CB_FROM_END		-1

extern ssize_t __log2cb_get_size(int fd);
extern int __log2cb_append_msg(int fd, char *msg, size_t len);
extern int __log2cb_read_data(int fd, char *buf, size_t len,
						off_t *off, int whence);
extern int __log2cb_read_msg(int fd, char *buf, size_t len,
						off_t *off, int whence);
extern int __log2cb_show_data(int fd, off_t *off, int whence);
extern int log2cb_get_data(int fd, char **ptr, size_t *len,
						off_t *off, int whence);

enum log2cb_open_mode_e {
	LOG2CB_OPEN_READONLY,	/* if exist then open read-only, else error */
	LOG2CB_OPEN_TRUNC,	/* if exist trunc, else create */
	LOG2CB_OPEN_TRUNC_NOSYNC,/* as LOG2CB_OPEN_TRUNC but without O_SYNC */
	LOG2CB_OPEN_CREATE,	/* if exist open read-write, else create */
	LOG2CB_OPEN,		/* if exist open read-write, else error */
	__LOG2CB_OPEN_END
};
extern int log2cb_open(const char *name, enum log2cb_open_mode_e mode,
							size_t size);
extern int log2cb_close(int fd);

#endif /* _LOG2CB_H */
