#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

echo -n "[it may take a (long) while...] "

writecb $NAME.log 6K < $NAME.data

# Old resize method: dump old file contents then create a new file with
# the desired size and fill it with the dumped data.
oldway="readcb $NAME.log > $NAME.res ; \
	writecb -I $NAME.exp 1K ; \
	writecb $NAME.exp < $NAME.res"
t=$(echo | time -f "%E" bash -c "$oldway" 2>&1)
echo -n "old method elapse ${t}s, " 
readcb $NAME.exp > $NAME.res

# New resize method
t=$(echo | time -f "%E" writecb -R $NAME.log 1K 2>&1)
echo -n "new method elapse ${t}s, " 
readcb $NAME.log > $NAME.exp

if diff -u $NAME.res $NAME.exp ; then
        echo OK.
        exit 0
else
        echo KO!
        exit 1
fi

exit 0
