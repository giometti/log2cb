#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

fail=n
echo "0123456" | writecb $NAME.log 32 || fail=y
echo "0123456" | writecb $NAME.log || fail=y

readcb $NAME.log > $NAME.res

cat <<__EOF > $NAME.exp
0123456
0123456
__EOF

if [ $fail = n ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
