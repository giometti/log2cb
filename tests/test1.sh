#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

fail=n
cat <<__EOF | writecb $NAME.log 32 || fail=y
0000000
11111111
AAAAAAAAAAAAAA
0000000
__EOF

readcb $NAME.log > $NAME.res

cat <<__EOF > $NAME.exp
AAAAAAAAAAAAAA
0000000
__EOF

if [ $fail = n ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
