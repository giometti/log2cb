#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

fail=n
echo "0123456" | writecb $NAME.log 32 2> $NAME.res || fail=y
echo "0123456" | writecb $NAME.log 64 -d 2>&1 | grep 'note:' >> $NAME.res || fail=y

cat <<__EOF > $NAME.exp
[writecb] log2cb_open: (maxsize != size) note: size mismatch! Passed 64, but got 32
__EOF

if [ $fail = n ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
