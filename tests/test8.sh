#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

echo -e "MESSAGE1\nMESSAGE2\nMESSAGE3" | writecb $NAME.log 128
../tools/cbheader set $NAME.log head=200

ret=0
readcb $NAME.log 2> $NAME.res || ret=$?

cat <<__EOF > $NAME.exp
[readcb] Invalid HEAD
__EOF

if [ $ret -eq 2 ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
