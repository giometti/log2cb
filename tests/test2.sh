#!/bin/bash

NAME=$(basename $0 .sh)

set -e

fail=n
readcb no.log 2> $NAME.res || fail=y

cat <<__EOF > $NAME.exp
[readcb] No such file or directory
__EOF

if [ $fail = y ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
