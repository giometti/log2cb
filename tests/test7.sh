#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

echo -n "[it may take a while...] "

fail=n
cat $NAME.data | writecb $NAME.log 8192 || fail=y

readcb $NAME.log > $NAME.res

if [ $fail = n ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
