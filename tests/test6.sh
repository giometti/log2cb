#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

fail=n
echo -e "MESSAGE1\nMESSAGE2\nMESSAGE3" | writecb $NAME.log 128 || fail=y
writecb -I $NAME.log 64 || fail=y
echo "MESSAGE1" | writecb $NAME.log || fail=y

readcb $NAME.log > $NAME.res

cat <<__EOF > $NAME.exp
MESSAGE1
__EOF

if [ $fail = n ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
