#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log 

writecb $NAME.log 1024 < $NAME.data

t=$(echo This is a new MESSAGE | time -f "%E" writecb -r $NAME.log 512 2>&1)
readcb $NAME.log > $NAME.res

if diff -u $NAME.res $NAME.exp ; then
        echo "elapse ${t}s, OK."
        exit 0
else
        echo KO!
        exit 1
fi
