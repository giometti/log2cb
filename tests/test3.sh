#!/bin/bash

NAME=$(basename $0 .sh)

set -e

rm -rf $NAME.log

fail=n
echo -n "0123456" | writecb $NAME.log 32 2> $NAME.res || fail=y

cat <<__EOF > $NAME.exp
[writecb] unable to write log: Invalid message
__EOF

if [ $fail = y ] && diff -u $NAME.res $NAME.exp ; then
	echo OK.
	exit 0
else
	echo KO!
	exit 1
fi
