#!/bin/bash

function do_exec ( ) {
	echo "\$ $@"
	bash -c "$@"
}

make -C .. || exit 1
export PATH+=":.."
export LD_LIBRARY_PATH=..

do_exec writecb -h
do_exec readcb -h

echo "---"

rm -f test.log

do_exec writecb test.log
do_exec "echo MESSAGE1 | writecb test.log 128"
do_exec "echo MESSAGE2 | writecb test.log 64"
do_exec "echo MESSAGE3 | writecb test.log"
do_exec readcb test.log

echo ""

do_exec "writecb -I test.log 64"
do_exec "echo MESSAGE1 | writecb test.log"
do_exec readcb test.log

echo "---"

do_exec "echo MESSAGE2 | writecb test.log 64"
do_exec readcb -l 14 test.log

exit 0
