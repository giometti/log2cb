/*
 * Copyright (C) 2022   Rodolfo Giometti <giometti@enneenne.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/inotify.h>

#include "misc.h"
#include "log2cb.h"

static int act_as_tail_f;
static size_t last_chars;
static char *logfile;

static inline void READ_LOCK(int fd)
{
	int ret;
        err_if_exit((ret = log2cb_read_lock(fd)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

static inline void READ_UNLOCK(int fd)
{
	int ret;
        err_if_exit((ret = log2cb_read_unlock(fd)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

static inline void __GET_HEAD(int fd, uint32_t *pvalue)
{
	int ret;
	err_if_exit((ret = __log2cb_get_head(fd, pvalue)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

static inline void __GET_MAXSIZE(int fd, uint32_t *pvalue)
{
	int ret;
	err_if_exit((ret = __log2cb_get_maxsize(fd, pvalue)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

static inline void __GET_FILESIZE(int fd, uint32_t *pvalue)
{
	int ret;
	err_if_exit((ret = __log2cb_get_filesize(fd, pvalue)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

/*
 * The main task
 */

static void dump_data(int fd)
{
	uint32_t maxsize, dsize;
	uint32_t ofilesize, filesize;
	off_t off = -last_chars;
	int id, wd;
	struct inotify_event event;
	int ret;

	READ_LOCK(fd);
	ret = __log2cb_show_data(fd, &off,
			off == 0 ? LOG2CB_FROM_BEGINNING : LOG2CB_FROM_END);
	err_if_exit(ret < 0, EXIT_FAILURE,
			"unable to read log: %s", log2cb_strerror(-ret));

	__GET_MAXSIZE(fd, &maxsize);
	__GET_FILESIZE(fd, &ofilesize);
	READ_UNLOCK(fd);

	if (act_as_tail_f) {
		/* Create the INOTIFY instance */
		id = inotify_init();
		err_if_exit(id < 0, EXIT_FAILURE, "cannot use INOTIFY: %m");

		/* Add the circular buffer into the watch list */
		wd = inotify_add_watch(id, logfile, IN_MODIFY);
		err_if_exit(wd < 0, EXIT_FAILURE, "cannot add watch: %m");

		while (1) {
			/*
			 * For the first round only: since we have released
			 * the lock some new messages may have been arrived
			 * before we added the circular buffer to the watch
			 * list, so keep reading (at least __log2cb_show_data()
			 * will display nothing - __log2cb_show_data() will
			 * return 0).
			 */

			READ_LOCK(fd);

			__GET_FILESIZE(fd, &filesize);
			/* Check if we are too slow in reading new messages */
			if (filesize > ofilesize)
				dsize = filesize - ofilesize;
			else 	/* filesize wrapped */
				dsize = 0xffffffff - ofilesize + filesize + 1;
			if (dsize >= maxsize) {
				err("too slow reading! Abort");
				exit(EXIT_FAILURE);
			}

			/* If dsize < maxsize then it's the exact amount of
			 * characters to be read from the end of file!
			 */
			off = -((off_t) dsize);
			ret = __log2cb_show_data(fd, &off, LOG2CB_FROM_END);
			err_if_exit(ret < 0, EXIT_FAILURE,
					"unable to read log: %s",
						log2cb_strerror(-ret));

			ofilesize = filesize;

			/*
			 * Now all new messages has been read, so we have to
			 * release the lock to allow writers to add new
			 * messages.
			 */

			READ_UNLOCK(fd);

			/*
			 * Now we can wait for file changes...
			 */

			ret = read(id, &event, sizeof(event));
			err_if_exit(ret < 0, EXIT_FAILURE,
						"cannot read events: %m");

			/*
			 * If we are here an event occourred to the file
			 * (an IN_MODIFY event for sure) so we can continue and
			 * check if we have something to print.
			 *
			 * Note that we don't need to use readn() to be sure
			 * we have read the whole struct inotify_event message,
			 * due to the fact we don't access to its internal
			 * data.
			 */
		}

		inotify_rm_watch(id, wd);
		close (id);
	}
}

/*
 * Usage
 */

static void usage(void)
{
        fprintf(stderr,
                "usage: %s [-h | --help] [-v | --version] [-d | --debug] [-t | --print-time]\n"
                "               [ -f ] [ --last | -l <n> ] <logfile>\n", NAME);

        exit(EXIT_FAILURE);
}

/*
 * Main
 */

int main(int argc, char *argv[])
{
        int c;
        struct option long_options[] = {
                { "help",               no_argument,            NULL, 'h'},
                { "version",		no_argument,		NULL, 'v'},
                { "debug",              no_argument,            NULL, 'd'},
                { "print-time",		no_argument,            NULL, 't'},
                { "last",		required_argument,      NULL, 'l'},
                { 0, 0, 0, 0    /* END */ }
        };
        int option_index = 0;
	int fd;
	int ret;

        /*
         * Parse options in command line
         */

        opterr = 0;          /* disbale default error message */
        while (1) {
                option_index = 0; /* getopt_long stores the option index here */

                c = getopt_long(argc, argv, "hvdtfl:",
                                long_options, &option_index);

                /* Detect the end of the options */
                if (c == -1)
                        break;

                switch (c) {
                case 'h':
                        usage();

		case 'v':
			info("log2cb - ver. %s", LOG2CB_VERSION);
			exit(EXIT_SUCCESS);

                case 'd':
                        __debug_level++;
                        break;

                case 't':
                        __add_time++;
                        break;

                case 'f':
                        act_as_tail_f++;
                        break;

                case 'l':
                        last_chars = atoi(optarg);
			err_if_exit(atoi == 0, EXIT_FAILURE,
				"invalid value for -l (must be greater than 0");
                        break;

                case ':':
                case '?':
                        err("invalid option %s", argv[optind - 1]);
                        exit(EXIT_FAILURE);

                default:
                        BUG();
                }
        }
        if (argc - optind < 1)
                usage();
        logfile = argv[optind];

        /*
         * System start-up.
         */

	ret = EXIT_FAILURE;
	fd = log2cb_open(logfile, LOG2CB_OPEN_READONLY, 0);
	if (log2cb_is_corrupted(-fd))
		ret = LOG2CB_PERROR_CORRUPTED;
	err_if_exit(fd < 0, ret, "%s", log2cb_strerror(-fd));

	/* Do the job */
	dump_data(fd);

	log2cb_close(fd);

        return 0;
}
