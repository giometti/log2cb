# Log2cb

This project implements a basic "circular buffer file" to store log messages.

## Feature

The goal is to limit the size of ever-growing log to a maximum amount, loosing in the process oldest entries if the amount is exceeded.

This behavior is really useful for "embedded" systems where logs are useful to keep track of system's events but after a specified size is desirable to lose old data.

So I decided to do this basic tool which can be used with already written logging daemons (such as syslog-ng, for instance) which provides:

* a maximum size for each log file.
* the ability to wait for new log messages at run-time as command `tail -f` does.

## Compile

To compile the program just use `make` as follow:

    $ make

You can use some variables to alter compilation such as:

* `DYNAMIC=n` to switch to static library
* `CROSS_COMPILE=aarch64-linux-gnu-` (or any other prefix) to select a cross compiler.

## Basic usage

Once compiling is done you should get two programs: `writecb` and `reabcb`:

    $ writecb -h
    usage: writecb [-h | --help] [-v | --version] [-d | --debug] [-t | --print-time]
                   [-I | --reinit-and-exit] [-i | --reinit]
                   [-R | --resize-and-exit] [-r | --resize] <logfile> [<logsize>]
    $ readcb -h
    usage: readcb [-h | --help] [-v | --version] [-d | --debug] [-t | --print-time]
                   [ -f ] [ --last | -l <n> ] <logfile>

`writecb` takes one or two arguments, a log filename and an option maximum log size and then it waits for new log messages on stdin. If the supplied log file doesn't exist and `logsize` is specified then it creates it with its maximum size to `logsize`, otheriwise the log file is opened if it already exists or error.

By using option argument `-I` or `--reinit-and-exit` and `-i` or `--reinit` we can alter the maximum size of an already created log file (in this case its content is lost). The differences between the twos is the former couple do the job and then exit.
On the other hand by using the *resize* counterparts we can do the same as above but without loosing the buffer contents.

`readcb` take the log filename to be read and then it dumps its content on stdout. If the `-f` option argument is supplied then `readcb` will work as tail -f does, that is after displaying all data in the log file it waits for new incoming messages whose will be displayed on stdout.

By using option argument `-l <n>` or `--last <n>` we can start reading data within the buffer from `n` characters from the end of data.

Both `writecb` and `readcb` will return:

* 0 on succes.
* 1 on generic failure.
* 2 if supplied `<logfile>` is corrupted.

### Examples

Here some simple usage examples:

    $ writecb test.log
    [writecb] (fd < 0) No such file or directory
    $ echo MESSAGE1 | writecb test.log 128
    $ echo MESSAGE2 | writecb test.log 64
    $ echo MESSAGE3 | writecb test.log
    $ readcb test.log
    MESSAGE1
    MESSAGE2
    MESSAGE3

Now we can change the log size (losing its previous content) by using:

    $ writecb -I test.log 64
    $ echo MESSAGE1 | writecb test.log
    $ readcb test.log
    MESSAGE1

To start reading data within the buffer from 14 character before the end, we can use the following:

    $ echo MESSAGE2 | writecb test.log 64
    $ readcb -l 14 test.log
    AGE1
    MESSAGE2

## syslog-ng

A very useful usage of the above programs is with syslog-ng. In fact we can tell to syslog-ng to store log messages into our circular buffer by using the folowing setting within `/etc/syslog-ng.conf`:

    destination my_dest {
        program("/usr/bin/writecb /var/log/my.cb.log 16K" template(my_templ));
    };

In this manner a 16K bytes circular buffer logging file `/var/log/my.cb.log` is created and messages are stored there.