/*
 * Copyright (C) 2022   Rodolfo Giometti <giometti@enneenne.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <log2cb.h>

/*
 * Usage
 */

static void usage(char *name)
{
  fprintf(stderr,
	  "usage: %s [-h | --help] [-v | --version] [-d | --debug]\n"
	  "       [-t | --print-time]", name);

	exit(EXIT_FAILURE);
}

/*
 * Main
 */

int main(int argc, char **argv)
{
        int c;
        struct option long_options[] = {
                { "help",               no_argument,            NULL, 'h'},
                { "version",            no_argument,            NULL, 'v'},
                { "debug",              no_argument,            NULL, 'd'},
                { "print-time",         no_argument,            NULL, 't'},
                { 0, 0, 0, 0    /* END */ }
        };
        int option_index = 0;
	int fd;

        /*
         * Parse options in command line
         */

        opterr = 0;          /* disbale default error message */
        while (1) {
                option_index = 0; /* getopt_long stores the option index here */

                c = getopt_long(argc, argv, "hvdt",
                                long_options, &option_index);

                /* Detect the end of the options */
                if (c == -1)
                        break;

                switch (c) {
                case 'h':
                        usage(argv[0]);

                case 'v':
                        printf("log2cb - ver. %s\n", LOG2CB_VERSION);
                        exit(EXIT_SUCCESS);

                case 'd':
                        __debug_level++;
                        break;

                case 't':
                        __add_time++;
                        break;

                case ':':
                case '?':
                        fprintf(stderr, "invalid option %s\n",
						argv[optind - 1]);
                        exit(EXIT_FAILURE);

                default:
			fprintf(stderr, "BUG\n");
                        exit(EXIT_FAILURE);
                }
        }

	fd = log2cb_open("/tmp/external_app.log", LOG2CB_OPEN_TRUNC, 0);
	if (fd < 0) {
        	fprintf(stderr, "open error: %s\n", log2cb_strerror(-fd));
		exit(EXIT_FAILURE);
	}
        log2cb_close(fd);

	return 0;
}
