/*
 * Copyright (C) 2022   Rodolfo Giometti <giometti@enneenne.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <fcntl.h>
#include <getopt.h>

#include "misc.h"
#include "log2cb.h"

/*
 * Generic functions
 */

static void read_header(int fd, struct log2cb_header_s *h)
{
	int ret;

	ret = __log2cb_get_magic(fd, &h->magic);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot read MAGIC: %m");
	ret = __log2cb_get_version(fd, &h->version);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot read VERSION: %m");
	ret = __log2cb_get_head(fd, &h->head);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot read HEAD: %m");
	ret = __log2cb_get_tail(fd, &h->tail);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot read TAIL: %m");
	ret = __log2cb_get_maxsize(fd, &h->maxsize);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot read MAXSIZE: %m");
	ret = __log2cb_get_filesize(fd, &h->filesize);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot read FILESIZE: %m");
}

/*
 * Command functions
 */

static void do_get(int argc, char **argv)
{
	int fd;
	struct log2cb_header_s h;

	/*
	 * We cannot use log2cb_open() due to the fact it will return error
	 * if the header is corrupted!
	 */
	fd = open(argv[0], O_RDONLY);
	err_if_exit(fd < 0, EXIT_FAILURE, "cannot open file: %m");

	/* Read and print the header */
	read_header(fd, &h);
	info("magic:    %08x", h.magic);
	info("version:  %08x(%u)", h.version, h.version >> 16);
	info("head:     %u", h.head);
	info("tail:     %u", h.tail);
	info("maxsize:  %u", h.maxsize);
	info("filesize: %u", h.filesize);

	close(fd);
}

static void do_set(int argc, char **argv)
{
	int fd;
	struct log2cb_header_s h;
	char *p = NULL;
	unsigned int v;
	int i, ret;

	/*
	 * We cannot use log2cb_open() due to the fact it will return error
	 * if the header is corrupted!
	 */
	fd = open(argv[0], O_RDWR);
	err_if_exit(fd < 0, EXIT_FAILURE, "cannot open file: %m");

	/* Read the header */
	read_header(fd, &h);

	/* Parse command line */
	for (i = 1; i < argc; i++) {
		ret = sscanf(argv[i], "%m[^=]=%u", &p, &v);
		err_if_exit(ret != 2, EXIT_FAILURE,
				"cannot parse '%s'", argv[i]);
		if (strcmp("head", p) == 0)
			ret = __log2cb_set_head(fd, v);
		else if (strcmp("head", p) == 0)
                        ret = __log2cb_set_head(fd, v);
		else if (strcmp("maxsize", p) == 0)
                        ret = __log2cb_set_maxsize(fd, v);
		else if (strcmp("filesize", p) == 0)
                        ret = __log2cb_set_filesize(fd, v);
		else {
			err("unknow parameter %s", p);
			exit(EXIT_FAILURE);
		}
	}
	free(p);

	close(fd);

}

/*
 * Usage
 */

static void usage(void)
{
  fprintf(stderr,
	  "usage: %s [-h | --help] [-v | --version] [-d | --debug]\n"
	  "       [-t | --print-time] <COMMAND>\n"
	  "  where <COOMAND> can be:\n"
	  "    - get <logfile>\n"
	  "    - set <logfile> [head=<head>] [tail=<tail>] [maxsize=<maxsize>]\n"
	  "          [filesize=<filesize>]\n",
            NAME);

	exit(EXIT_FAILURE);
}

/*
 * Main
 */

int main(int argc, char **argv)
{
        int c;
        struct option long_options[] = {
                { "help",               no_argument,            NULL, 'h'},
                { "version",            no_argument,            NULL, 'v'},
                { "debug",              no_argument,            NULL, 'd'},
                { "print-time",         no_argument,            NULL, 't'},
                { 0, 0, 0, 0    /* END */ }
        };
        int option_index = 0;
	char *cmd;


        /*
         * Parse options in command line
         */

        opterr = 0;          /* disbale default error message */
        while (1) {
                option_index = 0; /* getopt_long stores the option index here */

                c = getopt_long(argc, argv, "hvdtfl:",
                                long_options, &option_index);

                /* Detect the end of the options */
                if (c == -1)
                        break;

                switch (c) {
                case 'h':
                        usage();

                case 'v':
                        info("log2cb - ver. %s", LOG2CB_VERSION);
                        exit(EXIT_SUCCESS);

                case 'd':
                        __debug_level++;
                        break;

                case 't':
                        __add_time++;
                        break;

                case ':':
                case '?':
                        err("invalid option %s", argv[optind - 1]);
                        exit(EXIT_FAILURE);

                default:
                        BUG();
                }
        }
        if (argc - optind < 2)
                usage();
        cmd = argv[optind];

	/* Check command */
	if (strcmp(cmd, "get") == 0)
		do_get(argc - optind - 1, argv + optind + 1);
	else if (strcmp(cmd, "set") == 0)
		do_set(argc - optind - 1, argv + optind + 1);
	else {
		err("unknow command");
		exit(EXIT_FAILURE);
	}

	return 0;
}
