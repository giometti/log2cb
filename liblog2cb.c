/*
 * Copyright (C) 2022   Rodolfo Giometti <giometti@enneenne.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "misc.h"
#include "log2cb.h"

BUILD_BUG_ON(EXIT_FAILURE == 1, "EXIT_FAILURE is not defined as 1");

/*
 * Library's global variables
 */

int __debug_level;		/* the debug level */
int __add_time;			/* if > 0 prepend time on all messages */

/* The error strings */
const char *log2cb_error_str[] = {
        [LOG2CB_SUCCESS] = "Success",
        [LOG2CB_ERROR_ERRNO] = "%m",
        [LOG2CB_ERROR_CORRUPTED_LOGFILE] = "Corrupted log file",
        [LOG2CB_ERROR_INVALID_MAGIC] = "Invalid MAGIC",
        [LOG2CB_ERROR_INVALID_VERSION] = "Invalid VERSION",
        [LOG2CB_ERROR_INVALID_HEAD] = "Invalid HEAD",
        [LOG2CB_ERROR_INVALID_TAIL] = "Invalid TAIL",
        [LOG2CB_ERROR_INVALID_MODE] = "Invalid mode",
        [LOG2CB_ERROR_INVALID_MESSAGE] = "Invalid message",
        [LOG2CB_ERROR_TOO_LONG_MESSAGE] = "Too long message",
        [LOG2CB_ERROR_INVALID_OFFSET] = "Invalid offset",
        [__LOG2CB_ERROR_END] = "BUG"
};

/*
 * Private functions
 */

#define MIN_TIMEOUT_US		10000	/* FIXME */

static int readn(int fd, void *vptr, size_t n, unsigned long long t_ns)
{
	struct timeval t, to;
	fd_set set;
        int nleft = n, nread;
        char *ptr = vptr;
	int ret;

	if (t_ns > 0) {
		t.tv_sec = t_ns / 1000000000;
		t.tv_usec = t_ns / 1000;

		/*
		 * System granularity is about 1ms, so we should not take
		 * into account very small timeouts!
		 */
		if (t.tv_sec == 0 && t.tv_usec < MIN_TIMEOUT_US)
			t.tv_usec = MIN_TIMEOUT_US;
	}

        FD_ZERO(&set);
        FD_SET(fd, &set);
        while (nleft > 0) {
		if (t_ns > 0) {
			to = t;
			ret = select(fd + 1, &set, NULL, NULL, &to);
			if (ret < 0)
				return ret;
			if (ret == 0)
				return -ETIME;
			BUG_ON(ret != 1);

			/*
			 * Note that here we have:
			 *     - FD_SET(fd, &set) is not needed due to the fact
			 *       if ret is 1 this is the only fd set in the
			 *       variable, and
			 *     - "to" holds the the amount of time not slept
			 *       (Linux behaviour only).
			 */
		}

                nread = read(fd, ptr, nleft);
                if (nread < 0) {
                        if (errno != EAGAIN)
                                return -1;
                        nread = 0;
                } else if (nread == 0)
                        break;

                nleft -= nread;
                ptr += nread;
        }

        return n - nleft;
}

static int writen(int fd, const void *vptr, size_t n, unsigned long long t_ns)
{
	struct timeval t, to;
	fd_set set;
        int nleft = n, nwritten;
        char *ptr = (char *) vptr;
	int ret;

	if (t_ns > 0) {
		t.tv_sec = t_ns / 1000000000;
		t.tv_usec = t_ns / 1000;

		/*
		 * System granularity is about 1ms, so we should not take
		 * into account very small timeouts!
		 */
		if (t.tv_sec == 0 && t.tv_usec < MIN_TIMEOUT_US)
			t.tv_usec = MIN_TIMEOUT_US;
	}

        FD_ZERO(&set);
        FD_SET(fd, &set);

        while (nleft > 0) {
		if (t_ns > 0) {
			to = t;
			ret = select(fd + 1, NULL, &set, NULL, &to);
			if (ret < 0)
				return -1;
			if (ret == 0)
				return -ETIME;
			BUG_ON(ret != 1);

			/*
			 * Note that here we have:
			 *     - FD_SET(fd, &set) is not needed due to the fact
			 *       if ret is 1 this is the only fd set in the
			 *       variable, and
			 *     - "to" holds the the amount of time not slept
			 *       (Linux behaviour only).
			 */
		}

                nwritten = write(fd, ptr, nleft);
                if (nwritten < 0) {
                        if (errno != EAGAIN)
                                return -1;
                        nwritten = 0;
                } else if (nwritten == 0)
                        break;

                nleft -= nwritten;
                ptr += nwritten;
        }

        return n - nleft;
}

/*
 * Compute the circular buffer size.
 * Note that if head = tail then size = 0, while maximum value of size can be
 * maxsize - 1.
 */
static size_t compute_size(uint32_t head, uint32_t tail, uint32_t maxsize)
{
	size_t size;

        if (head >= tail)
                size = head - tail;
        else
                size = maxsize - tail + head;
	return size;
}

static int log2cb_do_lock(int fd, short type)
{
	struct flock fl;

	fl.l_type = type;
	fl.l_whence = SEEK_SET;
	fl.l_start = 0;
	fl.l_len = LOG2CB_HEADER_SIZE;
	fl.l_pid = 0; /* F_SETLK(W) ignores it; F_OFD_SETLK(W) requires it to be zero */

	return fcntl(fd, F_SETLKW, &fl);
}

static int log2cb_do_unlock(int fd)
{
	struct flock fl;

	fl.l_type = F_UNLCK;
	fl.l_whence = SEEK_SET;
	fl.l_start = 0;
	fl.l_len = LOG2CB_HEADER_SIZE;
	fl.l_pid = 0; /* F_SETLK(W) ignores it; F_OFD_SETLK(W) requires it to be zero */

	return fcntl(fd, F_SETLKW, &fl);
}

static int __log2cb_init_logfile(int fd, size_t maxsize)
{
        int ret;

        /*
	 * So let's start by writing all zeros at the beginning by using
	 * two ftruncate() calls.
	 * This is needed by new LOG2CB_OPEN_TRUNC mode where a log file may
	 * already exists and holding data. By using the first ftruncate() call
	 * we erase everything within the file and then, by using the second
	 * one, we fill the file with all zeros (as stated by man 2 ftruncate.
	 */
        ret = ftruncate(fd, 0);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
        ret = ftruncate(fd, maxsize + LOG2CB_HEADER_SIZE);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;

        /* Now we have to acquire the file lock */
        ret = log2cb_write_lock(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;

        /* Now we can safely initialize the header */
        ret = __log2cb_set_magic(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
        ret = __log2cb_set_version(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
        ret = __log2cb_set_maxsize(fd, maxsize);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;

        /* In the end we can release the file lock */
        ret = log2cb_write_unlock(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;

	return 0;
}

static int log2cb_init_logfile(int fd, size_t maxsize)
{
        int err, ret;

        ret = log2cb_write_lock(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;

        /* Now we can safely initialize the header */
        err = __log2cb_init_logfile(fd, maxsize);

        ret = log2cb_write_unlock(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;

	return err;
}

static int __log2cb_check_logfile(int fd)
{
        uint32_t magic, version, maxsize, head, tail;
        struct stat statbuf;
	int ret;

        ret = __log2cb_get_magic(fd, &magic);
        if (ret < 0)
                return ret;
        if (magic != LOG2CB_MAGIC) {
		dbg("wrong magic is %u", magic);
                return -LOG2CB_ERROR_INVALID_MAGIC;
	}
        ret = __log2cb_get_version(fd, &version);
        if (ret < 0)
                return ret;
        if (version != LOG2CB_VERSION_1) {
		dbg("wrong version is %u", version);
                return -LOG2CB_ERROR_INVALID_VERSION;
	}
        ret = __log2cb_get_maxsize(fd, &maxsize);
        if (ret < 0)
                return ret;

	ret = __log2cb_get_head(fd, &head);
	if (ret < 0)
		return ret;
	if (head > maxsize) {
		dbg("wrong head is %u (maxsize=%u)", head, maxsize);
		return -LOG2CB_ERROR_INVALID_HEAD;
	}
	ret = __log2cb_get_tail(fd, &tail);
	if (ret < 0)
		return ret;
	if (tail > maxsize) {
		dbg("wrong tail is %u (maxsize=%u)", tail, maxsize);
		return -LOG2CB_ERROR_INVALID_TAIL;
	}

	ret = fstat(fd, &statbuf);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;

	if (maxsize != statbuf.st_size - LOG2CB_HEADER_SIZE) {
		dbg("wrong file size is %lu-%lu (maxsize=%u)", statbuf.st_size,
					LOG2CB_HEADER_SIZE, maxsize);
		return -LOG2CB_ERROR_CORRUPTED_LOGFILE;
	}

	return 0;
}

static int __log2cb_get_free_space(int fd, uint32_t tail,
				uint32_t maxsize, size_t len)
{
	char buf[PAGE_SIZE];
	uint32_t space, size;
	int i, nread;
	int ret;

	dbg("tail=%d len=%ld", tail, len);
	space = 0;
	size = 0;
	while (size <= maxsize) {
		ret = log2cb_preadn(fd, buf, PAGE_SIZE,
					tail + LOG2CB_HEADER_SIZE, 0);
                if (ret < 0)
			return -LOG2CB_ERROR_ERRNO;
		nread = ret;

		for (i = 0; i < nread; i++) {
			space++;
			if (buf[i] == '\n' && space >= len) {
				tail = (tail + i + 1) % maxsize;
				if (__log2cb_set_tail(fd, tail) < 0)
					return -LOG2CB_ERROR_ERRNO;
				dbg("newtail=%d", tail);
				return tail;		
			}
		}

		tail = (tail + nread) % maxsize;
		size += nread;
	}

	/* We should NEVER get here! */
	BUG();
	return 0;
}

static int __log2cb_append_msg_safe(int fd, uint32_t head, uint32_t tail,
				uint32_t maxsize, char *msg, size_t len)
{
	uint32_t filesize;
	int n = 0;
	int ret;

	dbg("head=%d tail=%d len=%ld", head, tail, len);
	if (head + len > maxsize) {
		/* In this case we must split the message */
		n = maxsize - head;
		ret = log2cb_pwriten(fd, msg, n, head + LOG2CB_HEADER_SIZE, 0);
		if (ret < 0)
			return -LOG2CB_ERROR_ERRNO;

		head = (head + n) % maxsize;
	}
	ret = log2cb_pwriten(fd, msg + n, len - n, head + LOG2CB_HEADER_SIZE, 0);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
	dbg("tmphead=%d", head);

	/*
	 * Move head in order to state that the new message has been written.
	 *
	 * Note that if power fails before executing the following steps,
	 * we simply loose the last log message but the circular buffer
	 * integrity is still sane.
	 */
	head = (head + len - n) % maxsize;
	if (__log2cb_set_head(fd, head) < 0)
		return -LOG2CB_ERROR_ERRNO;
	dbg("newhead=%d", head);

	/*
	 * Move counter for "tail -f".
	 *
	 * Note that is operation is not atomic and it may happen that filesize
	 * is not correctly updated if power fails; however this is not fatal
	 * for circular buffer integrity since it's just used for a correct
	 * "tail -f" implementation.
	 */
        if (__log2cb_get_filesize(fd, &filesize) < 0)
		return -LOG2CB_ERROR_ERRNO;
	filesize += len;
        if (__log2cb_set_filesize(fd, filesize) < 0)
		return -LOG2CB_ERROR_ERRNO;

	return 0;
}

/*
 * Public functions
 */

const char *log2cb_strerror(enum log2cb_error_e error)
{
        BUG_ON(error < 0);
        BUG_ON(error >= __LOG2CB_ERROR_END);
        if (error == LOG2CB_ERROR_ERRNO)
                return strerror(errno);
        return log2cb_error_str[error];
}

int log2cb_preadn(int fd, void *vptr, size_t n, size_t off,
                                                unsigned long long t_ns)
{
        int ret;

        ret = lseek(fd, off, SEEK_SET);
        if (ret < 0)
                return ret;
        return readn(fd, vptr, n, t_ns);
}

int log2cb_pwriten(int fd, const void *vptr, size_t n, size_t off,
                                                unsigned long long t_ns)
{
        int ret;

        ret = lseek(fd, off, SEEK_SET);
        if (ret < 0)
                return ret;
        return writen(fd, vptr, n, t_ns);
}

int log2cb_write_lock(int fd)
{
	int ret;

	ret = log2cb_do_lock(fd, F_WRLCK);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
	return 0;
}

int log2cb_write_unlock(int fd)
{
	int ret;

	ret = log2cb_do_unlock(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
	return 0;
}

int log2cb_read_lock(int fd)
{
	int ret;

	ret = log2cb_do_lock(fd, F_RDLCK);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
	return 0;
}

int log2cb_read_unlock(int fd)
{
	int ret;

	ret = log2cb_do_unlock(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
	return 0;
}

ssize_t __log2cb_get_size(int fd)
{
	uint32_t head, tail, maxsize;

	if (__log2cb_get_head(fd, &head) < 0)
		return -LOG2CB_ERROR_ERRNO;
        if (__log2cb_get_tail(fd, &tail) < 0)
                return -LOG2CB_ERROR_ERRNO;
        if (__log2cb_get_maxsize(fd, &maxsize) < 0)
                return -LOG2CB_ERROR_ERRNO;

	return compute_size(head, tail, maxsize);
}

/*
 * Append the message in msg (with length len) in the circular buffer.
 * Buffer msg holds just __one__ message with a \n as last character.
 */

int __log2cb_append_msg(int fd, char *msg, size_t len)
{
	uint32_t maxsize, head, tail, size;
	int ret;

	dbg("len=%ld", len);
	/* Sanity check */
	ret = __log2cb_check_logfile(fd);
	if (ret < 0)
                return ret;
	if (msg[len - 1] != '\n')
		return -LOG2CB_ERROR_INVALID_MESSAGE;
        if (__log2cb_get_maxsize(fd, &maxsize) < 0)
		return -LOG2CB_ERROR_ERRNO;
	if (len > maxsize - 1)
		return -LOG2CB_ERROR_TOO_LONG_MESSAGE;
        if (__log2cb_get_head(fd, &head) < 0)
		return -LOG2CB_ERROR_ERRNO;
        if (__log2cb_get_tail(fd, &tail) < 0)
		return -LOG2CB_ERROR_ERRNO;

	size = compute_size(head, tail, maxsize);
	dbg("size=%d", size);

	/*
	 * Before actually writing data we must reserve enought space for
	 * the new message.
	 */
	if (size + len >= maxsize)
		tail = __log2cb_get_free_space(fd, tail, maxsize,
					size + len + 1 - maxsize);

	/*
	 * Now we can add the new message.
	 *
	 * Note that if power fails before executing the following step,
	 * we simply loose the oldest log message (and the new one) but the
	 * circular buffer integrity is still sane.
	 */
	return __log2cb_append_msg_safe(fd, head, tail, maxsize, msg, len);
}

/*
 * Read at maximum len data from the buffer, starting from *off (according to
 * the directive whence), and put them in buf, returning how many bytes has
 * been stored in buf (0 means End-of-Data).
 *
 * The directive whence can be:
 *
 *     - LOG2CB_FROM_BEGINNING: the offset is computed starting from the
 *                              beginning of data.
 *
 *     - LOG2CB_FROM_END:       the offset is computed starting from the
 *                              end of data.
 *
 * Note that we cannot have:
 *      whence == LOG2CB_FROM_BEGINNING && *off < 0 || \
 *      whence == LOG2CB_FROM_END && *off > 0
 *
 * On the other hand, if whence == LOG2CB_FROM_BEGINNING and *off >= size
 * (where size is the total characters currently stored in the buffer)
 * the function will set *off = size and return 0 (End-of-Data), while if
 * whence == LOG2CB_FROM_END and abs(*off) > size, the function will set
 * *off = -size and then start returning the data.
 */

int __log2cb_read_data(int fd, char *buf, size_t len, off_t *off, int whence)
{
	uint32_t maxsize, head, tail, size;
	int nread;
	int ret;

	/* Sanity check */
	if ((whence == LOG2CB_FROM_BEGINNING && *off < 0) || \
	    (whence == LOG2CB_FROM_END && *off > 0))
		return -LOG2CB_ERROR_INVALID_OFFSET;
	ret = __log2cb_check_logfile(fd);
	if (ret < 0)
                return ret;
        if (__log2cb_get_maxsize(fd, &maxsize) < 0)
		return -LOG2CB_ERROR_ERRNO;
	if (__log2cb_get_head(fd, &head) < 0)
		return -LOG2CB_ERROR_ERRNO;
	if (__log2cb_get_tail(fd, &tail) < 0)
		return -LOG2CB_ERROR_ERRNO;
	size = compute_size(head, tail, maxsize);
	if (whence == LOG2CB_FROM_BEGINNING && *off >= size)
		*off = size;
	else if (whence == LOG2CB_FROM_END && abs(*off) > size)
		*off = -((off_t) size);
	dbg("off=%ld", *off);

	/* Move tail according to off */
	if (whence == LOG2CB_FROM_BEGINNING && *off >= 0)
		tail = (tail + *off) % maxsize;
	else if (whence == LOG2CB_FROM_END && *off <= 0)
		tail = (tail + size + *off) % maxsize;
	else
		BUG();
	dbg("tail=%d", tail);

	nread = 0;
	if (head < tail) {
		/* In this case we have to read from tail to EOF */
		ret = log2cb_preadn(fd, buf, len,
				tail + LOG2CB_HEADER_SIZE, 0);
		if (ret < 0)
			return -LOG2CB_ERROR_ERRNO;
		nread = ret;
	} else {
		/* In this case have to read from tail till head - 1 */
		size = head - tail;
		dbg("size=%d", size);
		if (size) {
			ret = log2cb_preadn(fd, buf, min(len, size),
					tail + LOG2CB_HEADER_SIZE, 0);
			if (ret < 0)
				return -LOG2CB_ERROR_ERRNO;
			nread = ret;
		}
	}

	return nread;
}

/*
 * Read __one__ message from the buffer, starting from *off (according to
 * the directive whence), and put them in buf, returning how many bytes has
 * been stored in buf (0 means End-of-Data).
 * If the supplied buffer is too small to hold the message the function will
 * return -LOG2CB_ERROR_TOO_LONG_MESSAGE.
 *
 * The directive whence can be:
 *
 *     - LOG2CB_FROM_BEGINNING: the offset is computed starting from the
 *                              beginning of data.
 *
 *     - LOG2CB_FROM_END:       the offset is computed starting from the
 *                              end of data.
 *
 * Note that we cannot have:
 *      whence == LOG2CB_FROM_BEGINNING && *off < 0 || \
 *      whence == LOG2CB_FROM_END && *off > 0
 *
 * On the other hand, if whence == LOG2CB_FROM_BEGINNING and *off >= size
 * (where size is the total characters currently stored in the buffer)
 * the function will set *off = size and return 0 (End-of-Data), while if
 * whence == LOG2CB_FROM_END and abs(*off) > size, the function will set
 * *off = -size and then start returning the data.
 */

int __log2cb_read_msg(int fd, char *buf, size_t len, off_t *off, int whence)
{
	uint32_t maxsize, head, tail, size;
	int nread;
	int ret;

	/* Sanity check */
	if ((whence == LOG2CB_FROM_BEGINNING && *off < 0) || \
	    (whence == LOG2CB_FROM_END && *off > 0))
		return -LOG2CB_ERROR_INVALID_OFFSET;
	ret = __log2cb_check_logfile(fd);
	if (ret < 0)
                return ret;
        if (__log2cb_get_maxsize(fd, &maxsize) < 0)
		return -LOG2CB_ERROR_ERRNO;
	if (__log2cb_get_head(fd, &head) < 0)
		return -LOG2CB_ERROR_ERRNO;
	if (__log2cb_get_tail(fd, &tail) < 0)
		return -LOG2CB_ERROR_ERRNO;
	size = compute_size(head, tail, maxsize);
	if (whence == LOG2CB_FROM_BEGINNING && *off >= size)
		*off = size;
	else if (whence == LOG2CB_FROM_END && abs(*off) > size)
		*off = -((off_t) size);
	dbg("off=%ld", *off);

	/* Move tail according to off */
	if (whence == LOG2CB_FROM_BEGINNING && *off >= 0)
		tail = (tail + *off) % maxsize;
	else if (whence == LOG2CB_FROM_END && *off <= 0)
		tail = (tail + size + *off) % maxsize;
	else
		BUG();
	dbg("head=%d tail=%d", head, tail);

	nread = 0;
	while ((nread < len) && (head != tail)) {
		ret = log2cb_preadn(fd, &buf[nread], 1,
				tail + LOG2CB_HEADER_SIZE, 0);
		if (ret < 0)
			return -LOG2CB_ERROR_ERRNO;
		if (buf[nread++] == '\n')
			return nread;
		tail = (tail + 1) % maxsize;
	}

	return head == tail ? 0 : -LOG2CB_ERROR_TOO_LONG_MESSAGE;
}

/*
 * Show all data within circular buffer starting from *off (according to
 * the directive whence - see __log2cb_read_data()).
 */

int __log2cb_show_data(int fd, off_t *off, int whence)
{
	char buf[PAGE_SIZE];
	int nread;
	int ret;

	while (1) {
		ret = __log2cb_read_data(fd, buf, PAGE_SIZE, off, whence);
		if (ret == 0)
			break;
		if (ret < 0)
			return ret;
		nread = ret;

		ret = writen(STDOUT_FILENO, buf, nread, 0);
		if (ret < 0)
			return -LOG2CB_ERROR_ERRNO;

		*off += nread;
	}

	return 0;
}

/*
 * Get all data within circular buffer starting from off (according to
 * the directive whence - see __log2cb_read_data()) and put them within
 * *ptr. If *ptr  is set to NULL and *len is set 0 before the call, then the
 * function will allocate a buffer for storing the line. This buffer should
 * be freed by the caller program when finished (or error).
 *
 * The function returns how many data has been written within ptr (or < 0 if
 * error) and its size in *len.
 */

int log2cb_get_data(int fd, char **ptr, size_t *len, off_t *off, int whence)
{
	char buf[PAGE_SIZE];
	int nread, nwritten, err;
	void *pret;
	int ret;

	ret = log2cb_read_lock(fd);
	if (ret < 0)
		return ret;

	nwritten = 0;
	while (1) {
		ret = __log2cb_read_data(fd, buf, PAGE_SIZE, off, whence);
		if (ret == 0)
			break;
		if (ret < 0) {
			err = ret;
			goto unlock;
		}
		nread = ret;

		if (nwritten + nread > *len) {
			pret = realloc(*ptr, *len + nread);
			if (!pret) {
				err = -LOG2CB_ERROR_ERRNO;
				goto unlock;
			}
			*ptr = pret;
			*len += nread;
		}
		memcpy(*ptr + nwritten, buf, nread);

		nwritten += nread;
		*off += nread;
	}
	err = nwritten;

unlock:
	ret = log2cb_read_unlock(fd);
	if (ret < 0)
		return ret;

	return err;
}

int log2cb_open(const char *name, enum log2cb_open_mode_e mode, size_t size)
{
	int flags;
        int fd;
        struct stat statbuf;
	uint32_t maxsize;
        int ret;

	switch (mode) {
	case LOG2CB_OPEN_TRUNC_NOSYNC:
		flags = O_CREAT | O_RDWR;
		break;
	case LOG2CB_OPEN_TRUNC:
	case LOG2CB_OPEN_CREATE:
		flags = O_CREAT | O_RDWR | O_SYNC;
		break;
	case LOG2CB_OPEN:
		flags = O_RDWR | O_SYNC;
		break;
	case LOG2CB_OPEN_READONLY:
		flags = O_RDONLY;
		break;
	default:
		return -LOG2CB_ERROR_INVALID_MODE;
	}

        ret = open(name, flags, 0660);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
        fd = ret;

        ret = fstat(fd, &statbuf);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
	switch (mode) {
	case LOG2CB_OPEN_TRUNC_NOSYNC:
	case LOG2CB_OPEN_TRUNC:
		/* Force (re)init log file */
		ret = log2cb_init_logfile(fd, size);
		if (ret < 0)
			return ret;
		break;
	case LOG2CB_OPEN_CREATE:
		if (statbuf.st_size == 0) {
			ret = log2cb_init_logfile(fd, size);
			if (ret < 0)
				return ret;
		}
		break;
	default:
		/* nop */;
	}

        /* Sanity checks */
        ret = __log2cb_check_logfile(fd);
	if (ret < 0)
		return ret;
        ret = __log2cb_get_maxsize(fd, &maxsize);
	if (ret < 0)
		return ret;
        switch (mode) {
	case LOG2CB_OPEN_TRUNC_NOSYNC:
	case LOG2CB_OPEN_TRUNC:
		if (maxsize != size)
			return -LOG2CB_ERROR_CORRUPTED_LOGFILE;
        case LOG2CB_OPEN_CREATE:
                dbg_if(maxsize != size,
                        "note: size mismatch! Passed %ld, but got %d",
                                                        size, maxsize);
                break;
        default:
                /* nop */;
        }

        return fd;
}

int log2cb_close(int fd)
{
	int ret;

	ret = close(fd);
	if (ret < 0)
		return -LOG2CB_ERROR_ERRNO;
	return 0;
}
