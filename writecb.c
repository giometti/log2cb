/*
 * Copyright (C) 2022   Rodolfo Giometti <giometti@enneenne.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "misc.h"
#include "log2cb.h"

static int reinit, resize, and_exit;

static inline void READ_LOCK(int fd)
{
	int ret;
	err_if_exit((ret = log2cb_read_lock(fd)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

static inline void READ_UNLOCK(int fd)
{
	int ret;
	err_if_exit((ret = log2cb_read_unlock(fd)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

static inline void WRITE_LOCK(int fd)
{
	int ret;
        err_if_exit((ret = log2cb_write_lock(fd)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

static inline void WRITE_UNLOCK(int fd)
{
	int ret;
        err_if_exit((ret = log2cb_write_unlock(fd)) < 0, EXIT_FAILURE,
				"%s", log2cb_strerror(-ret));
}

/*
 * Parse filesize option argument in the form <number>[(k|K)|(m|M)], so
 * strings as: 1048576, 1024K or 1m mean 1048576 bytes.
 */

static size_t parse_filesize(const char *str)
{
	unsigned int size;
	long long lsize;
	char unit;
	int ret;

	ret = sscanf(str, "%u%c", &size, &unit);
	if (ret < 0)
		return ret;
	if (ret == 1 && size > 0)
		return size;
	if (ret == 2 && size > 0) {
		lsize = size;
		switch (unit) {
		case 'k':
		case 'K': lsize <<= 10; break;
		case 'm':
		case 'M': lsize <<= 20; break;
		default:
			return 0;
		}
		if (lsize > (2LL << 30))	/* invalid if > 2G */
			return 0;
		return (size_t) lsize;
	}
	return 0;	/* 0 means ERROR! */
}

static int append_msg(int fd, char *msg, size_t len)
{
        int ret;
        WRITE_LOCK(fd);
        ret = __log2cb_append_msg(fd, msg, len);
        WRITE_UNLOCK(fd);
        return ret;
}

static void do_resize(char *logfile, size_t logsize)
{
	char *logbak, *lognew;
	int fd, fdnew;
	uint32_t maxsize;
	ssize_t size, nread;
	off_t off;
	int skip_1st = 0;
	char *msg = NULL;
	size_t msgsize = PAGE_SIZE;
	int ret;

	ret = asprintf(&logbak, "%s.backup", logfile);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot allocate memory");

	ret = access(logfile, F_OK);
	if (ret != 0) {
		/*
		 * If logfile doesn't exist...
		 */
		ret = access(logbak, F_OK);
		if (ret == 0) {
			/*
			 * ...but logfile.backup does, then rename
			 * logfile.backup to logfile and exit.
			 */
			ret = rename(logbak, logfile);
			err_if_exit(ret < 0, EXIT_FAILURE,
				"cannot rename file %s", logbak);
		} else {
			/*
			 * ...and logfile.backup too, then create an empty
			 * logfile of size logsize and exit.
			 */
			reinit++;
		}
		free(logbak);
		return;
	}

	/*
	 * Otherwise, if logfile exists...
	 */

	ret = asprintf(&lognew, "%s.resized", logfile);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot allocate memory");

        fd = log2cb_open(logfile, LOG2CB_OPEN_READONLY, 0);
        if (log2cb_is_corrupted(-fd))
                ret = LOG2CB_PERROR_CORRUPTED;
        err_if_exit(fd < 0, ret, "%s: %s", logfile, log2cb_strerror(-fd));
	READ_LOCK(fd);

        ret = __log2cb_get_maxsize(fd, &maxsize);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot read log size");
	if (logsize == maxsize) {
		/* ...and logsize == maxsize, then do nothing */
		return;
	}

	/*
	 * Otherwise, do the resize...
	 */

	msg = realloc(msg, msgsize);
        err_if_exit(!msg, EXIT_FAILURE, "cannot allocate memory");

        fdnew = log2cb_open(lognew, LOG2CB_OPEN_TRUNC_NOSYNC, logsize);
        if (log2cb_is_corrupted(-fdnew))
                ret = LOG2CB_PERROR_CORRUPTED;
        err_if_exit(fdnew < 0, ret, "%s: %s", logfile, log2cb_strerror(-fdnew));

	size = __log2cb_get_size(fd);
        err_if_exit(size < 0, ret, "%s: %s", logfile, log2cb_strerror(-fdnew));
        off = size - logsize;
        if (off < 0) {
                off = 0;
	} else {
		/* If off > 0 it means that the 1st message, most probably, will
		 * be truncated, so let's skip it!
		 */
		skip_1st = ~0;
	}
	while (1) {
		ret = __log2cb_read_msg(fd, msg, msgsize,
					&off, LOG2CB_FROM_BEGINNING);
		if (ret == 0)
			break;
		if (ret == -LOG2CB_ERROR_TOO_LONG_MESSAGE) {
			msgsize += PAGE_SIZE;
			msg = realloc(msg, msgsize);
			err_if_exit(!msg, EXIT_FAILURE,
						"cannot allocate memory");
			continue;
		}
		err_if_exit(ret < 0, EXIT_FAILURE, "%s", log2cb_strerror(-ret));
		nread = ret;

		off += nread;
		if (skip_1st) {
			skip_1st = 0;
			continue;
		}

		ret = __log2cb_append_msg(fdnew, msg, nread);
		err_if_exit(ret < 0, EXIT_FAILURE, "%s", log2cb_strerror(-ret));
	}
	free(msg);

	READ_UNLOCK(fd);
        log2cb_close(fd);

	ret = fsync(fdnew);
        err_if_exit(ret < 0, EXIT_FAILURE, "%m");
        log2cb_close(fdnew);

	ret = rename(logfile, logbak);
	err_if_exit(ret < 0, EXIT_FAILURE, "cannot rename file %s", logfile);
        ret = rename(lognew, logfile);
        err_if_exit(ret < 0, EXIT_FAILURE, "cannot rename resized file %s! " \
				"Old data are in %s", lognew, logbak);

	free(logbak);
	free(lognew);
}

/*
 * The main loop
 */

static void mainloop(int fd)
{
	char *line = NULL;
	size_t len = 0;
	ssize_t nread;
	int ret;

	while ((nread = getline(&line, &len, stdin)) != -1) {
		ret = append_msg(fd, line, nread);
		err_if_exit(ret < 0, EXIT_FAILURE,
			"unable to write log: %s", log2cb_strerror(-ret));
	}
}

/*
 * Usage
 */

static void usage(void)
{
        fprintf(stderr,
                "usage: %s [-h | --help] [-v | --version] [-d | --debug] [-t | --print-time]\n"
                "               [-I | --reinit-and-exit] [-i | --reinit]\n"
                "               [-R | --resize-and-exit] [-r | --resize] <logfile> [<logsize>]\n", NAME);

        exit(EXIT_FAILURE);
}

/*
 * Main
 */

int main(int argc, char *argv[])
{
        int c;
        struct option long_options[] = {
                { "help",               no_argument,            NULL, 'h'},
                { "version",		no_argument,		NULL, 'v'},
                { "debug",              no_argument,            NULL, 'd'},
                { "print-time",		no_argument,            NULL, 't'},
                { "reinit",		no_argument,		NULL, 'i'},
                { "reinit-and-exit",	no_argument,		NULL, 'I'},
                { "resize",		no_argument,		NULL, 'r'},
                { "resize-and-exit",	no_argument,		NULL, 'R'},
                { 0, 0, 0, 0    /* END */ }
        };
        int option_index = 0;
        char *logfile;
	enum log2cb_open_mode_e mode = LOG2CB_OPEN;
	size_t logsize = 0;
	int fd;
	int ret;

        /*
         * Parse options in command line
         */

        opterr = 0;          /* disbale default error message */
        while (1) {
                option_index = 0; /* getopt_long stores the option index here */

                c = getopt_long(argc, argv, "hvdtiIrR",
                                long_options, &option_index);

                /* Detect the end of the options */
                if (c == -1)
                        break;

                switch (c) {
                case 'h':
                        usage();

                case 'v':
			info("log2cb - ver. %s", LOG2CB_VERSION);
			exit(EXIT_SUCCESS);

                case 'd':
                        __debug_level++;
                        break;

                case 't':
                        __add_time++;
                        break;

                case 'i':
			reinit++;
                        break;

                case 'I':
			reinit++;
			and_exit++;
                        break;

                case 'r':
			resize++;
                        break;

                case 'R':
			resize++;
			and_exit++;
                        break;

                case ':':
                case '?':
                        err("invalid option %s", argv[optind - 1]);
                        exit(EXIT_FAILURE);

                default:
                        BUG();
                }
        }
	if (reinit && resize) {
		err("cannot provide both resize and reinit functions");
		exit(EXIT_FAILURE);
	}
	if ((resize || reinit) && argc - optind < 2) {
		err("must provide <logsize> with resize or reinit functions");
		exit(EXIT_FAILURE);
	}
        if (argc - optind < 1)
                usage();
        logfile = argv[optind];
	if (argc - optind == 2) {
		logsize = parse_filesize(argv[optind + 1]);
		err_if_exit(logsize == 0, EXIT_FAILURE, "invalid filesize");
		dbg("filesize is %lubytes", logsize);
	}

        /*
         * System start-up.
         */

	if (resize)
		do_resize(logfile, logsize);
	mode = reinit ? LOG2CB_OPEN_TRUNC : LOG2CB_OPEN_CREATE;

	ret = EXIT_FAILURE;
	fd = log2cb_open(logfile, mode, logsize);
	if (log2cb_is_corrupted(-fd))
		ret = LOG2CB_PERROR_CORRUPTED;
	err_if_exit(fd < 0, ret, "%s", log2cb_strerror(-fd));

        /* Continue within the main loop if shouldn't exit */
	if (!and_exit)
		mainloop(fd);

	log2cb_close(fd);

        return 0;
}
